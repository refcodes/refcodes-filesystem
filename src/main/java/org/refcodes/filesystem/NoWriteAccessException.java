// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.filesystem;

import org.refcodes.filesystem.FileSystemException.FileException.NoAccessException;

/**
 * The Class {@link NoWriteAccessException}.
 */
public class NoWriteAccessException extends NoAccessException {

	private static final long serialVersionUID = 1L;

	/**
	 * {@inheritDoc}
	 */
	public NoWriteAccessException( String aMessage, FileHandle aFileHandle, String aErrorCode ) {
		super( aMessage, aFileHandle, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public NoWriteAccessException( String aMessage, FileHandle aFileHandle, Throwable aCause, String aErrorCode ) {
		super( aMessage, aFileHandle, aCause, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public NoWriteAccessException( String aMessage, FileHandle aFileHandle, Throwable aCause ) {
		super( aMessage, aFileHandle, aCause );
	}

	/**
	 * {@inheritDoc}
	 */
	public NoWriteAccessException( String aMessage, FileHandle aFileHandle ) {
		super( aMessage, aFileHandle );
	}

	/**
	 * {@inheritDoc}
	 */
	public NoWriteAccessException( FileHandle aFileHandle, Throwable aCause, String aErrorCode ) {
		super( aFileHandle, aCause, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public NoWriteAccessException( FileHandle aFileHandle, Throwable aCause ) {
		super( aFileHandle, aCause );
	}
}
