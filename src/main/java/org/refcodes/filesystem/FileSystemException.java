// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.filesystem;

import org.refcodes.exception.AbstractException;
import org.refcodes.mixin.KeyAccessor;
import org.refcodes.mixin.NameAccessor;
import org.refcodes.mixin.PathAccessor;

/**
 * The Class {@link FileSystemException}.
 */
public abstract class FileSystemException extends AbstractException {

	private static final long serialVersionUID = 1L;

	/**
	 * {@inheritDoc}
	 */
	public FileSystemException( String aMessage, String aErrorCode ) {
		super( aMessage, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public FileSystemException( String aMessage, Throwable aCause, String aErrorCode ) {
		super( aMessage, aCause, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public FileSystemException( String aMessage, Throwable aCause ) {
		super( aMessage, aCause );
	}

	/**
	 * {@inheritDoc}
	 */
	public FileSystemException( String aMessage ) {
		super( aMessage );
	}

	/**
	 * {@inheritDoc}
	 */
	public FileSystemException( Throwable aCause, String aErrorCode ) {
		super( aCause, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public FileSystemException( Throwable aCause ) {
		super( aCause );
	}

	/**
	 * The Class {@link FileException}.
	 */
	protected abstract static class FileException extends FileSystemException implements FileHandleAccessor {

		private static final long serialVersionUID = 1L;

		protected FileHandle _fileHandle;

		/**
		 * {@inheritDoc}
		 * 
		 * @param aFileHandle The file (handle) involved in this exception.
		 */
		public FileException( String aMessage, FileHandle aFileHandle, String aErrorCode ) {
			super( aMessage, aErrorCode );
			_fileHandle = aFileHandle;
		}

		/**
		 * {@inheritDoc}
		 * 
		 * @param aFileHandle The file (handle) involved in this exception.
		 */
		public FileException( String aMessage, FileHandle aFileHandle, Throwable aCause, String aErrorCode ) {
			super( aMessage, aCause, aErrorCode );
			_fileHandle = aFileHandle;
		}

		/**
		 * {@inheritDoc}
		 * 
		 * @param aFileHandle The file (handle) involved in this exception.
		 */
		public FileException( String aMessage, FileHandle aFileHandle, Throwable aCause ) {
			super( aMessage, aCause );
			_fileHandle = aFileHandle;
		}

		/**
		 * {@inheritDoc}
		 * 
		 * @param aFileHandle The file (handle) involved in this exception.
		 */
		public FileException( String aMessage, FileHandle aFileHandle ) {
			super( aMessage );
			_fileHandle = aFileHandle;
		}

		/**
		 * {@inheritDoc}
		 *
		 * @param aFileHandle The file (handle) involved in this exception.
		 */
		public FileException( FileHandle aFileHandle, Throwable aCause, String aErrorCode ) {
			super( aCause, aErrorCode );
			_fileHandle = aFileHandle;
		}

		/**
		 * {@inheritDoc}
		 *
		 * @param aFileHandle The file (handle) involved in this exception.
		 */
		public FileException( FileHandle aFileHandle, Throwable aCause ) {
			super( aCause );
			_fileHandle = aFileHandle;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public FileHandle getFileHandle() {
			return _fileHandle;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Object[] getPatternArguments() {
			return new Object[] { _fileHandle };
		}

		/**
		 * The Class {@link NoAccessException}.
		 */
		protected abstract static class NoAccessException extends FileException {

			private static final long serialVersionUID = 1L;

			/**
			 * {@inheritDoc}
			 */
			public NoAccessException( String aMessage, FileHandle aFileHandle, String aErrorCode ) {
				super( aMessage, aFileHandle, aErrorCode );
			}

			/**
			 * {@inheritDoc}
			 */
			public NoAccessException( String aMessage, FileHandle aFileHandle, Throwable aCause, String aErrorCode ) {
				super( aMessage, aFileHandle, aCause, aErrorCode );
			}

			/**
			 * {@inheritDoc}
			 */
			public NoAccessException( String aMessage, FileHandle aFileHandle, Throwable aCause ) {
				super( aMessage, aFileHandle, aCause );
			}

			/**
			 * {@inheritDoc}
			 */
			public NoAccessException( String aMessage, FileHandle aFileHandle ) {
				super( aMessage, aFileHandle );
			}

			/**
			 * {@inheritDoc}
			 */
			public NoAccessException( FileHandle aFileHandle, Throwable aCause, String aErrorCode ) {
				super( aFileHandle, aCause, aErrorCode );
			}

			/**
			 * {@inheritDoc}
			 */
			public NoAccessException( FileHandle aFileHandle, Throwable aCause ) {
				super( aFileHandle, aCause );
			}

		}
	}

	/**
	 * The Class {@link NoFileSystemAccessException}.
	 */
	protected abstract static class NoFileSystemAccessException extends FileSystemException {

		private static final long serialVersionUID = 1L;

		/**
		 * {@inheritDoc}
		 */
		public NoFileSystemAccessException( String aMessage, String aErrorCode ) {
			super( aMessage, aErrorCode );
		}

		/**
		 * {@inheritDoc}
		 */
		public NoFileSystemAccessException( String aMessage, Throwable aCause, String aErrorCode ) {
			super( aMessage, aCause, aErrorCode );
		}

		/**
		 * {@inheritDoc}
		 */
		public NoFileSystemAccessException( String aMessage, Throwable aCause ) {
			super( aMessage, aCause );
		}

		/**
		 * {@inheritDoc}
		 */
		public NoFileSystemAccessException( String aMessage ) {
			super( aMessage );
		}

		/**
		 * {@inheritDoc}
		 */
		public NoFileSystemAccessException( Throwable aCause, String aErrorCode ) {
			super( aCause, aErrorCode );
		}

		/**
		 * {@inheritDoc}
		 */
		public NoFileSystemAccessException( Throwable aCause ) {
			super( aCause );
		}
	}

	/**
	 * The Class {@link KeyException}.
	 */
	protected abstract static class KeyException extends FileSystemException implements KeyAccessor<String> {

		private static final long serialVersionUID = 1L;

		protected String _key;

		/**
		 * {@inheritDoc}
		 * 
		 * @param aKey The key involved in this exception.
		 */
		public KeyException( String aMessage, String aKey, String aErrorCode ) {
			super( aMessage, aErrorCode );
			_key = aKey;
		}

		/**
		 * {@inheritDoc}
		 * 
		 * @param aKey The key involved in this exception.
		 */
		public KeyException( String aMessage, String aKey, Throwable aCause, String aErrorCode ) {
			super( aMessage, aCause, aErrorCode );
			_key = aKey;
		}

		/**
		 * {@inheritDoc}
		 * 
		 * @param aKey The key involved in this exception.
		 */
		public KeyException( String aMessage, String aKey, Throwable aCause ) {
			super( aMessage, aCause );
			_key = aKey;
		}

		/**
		 * {@inheritDoc}
		 * 
		 * @param aKey The key involved in this exception.
		 */
		public KeyException( String aMessage, String aKey ) {
			super( aMessage );
			_key = aKey;
		}

		/**
		 * {@inheritDoc}
		 *
		 * @param aKey The key involved in this exception.
		 */
		public KeyException( String aKey, Throwable aCause, String aErrorCode ) {
			super( aCause, aErrorCode );
			_key = aKey;
		}

		/**
		 * {@inheritDoc}
		 *
		 * @param aKey The key involved in this exception.
		 */
		public KeyException( String aKey, Throwable aCause ) {
			super( aCause );
			_key = aKey;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public String getKey() {
			return _key;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Object[] getPatternArguments() {
			return new Object[] { _key };
		}
	}

	/**
	 * The Class {@link NameException}.
	 */
	protected abstract static class NameException extends FileSystemException implements NameAccessor {

		private static final long serialVersionUID = 1L;
		protected String _name;

		/**
		 * {@inheritDoc}
		 * 
		 * @param aName The name involved in this exception.
		 */
		public NameException( String aMessage, String aName, String aErrorCode ) {
			super( aMessage, aErrorCode );
			_name = aName;
		}

		/**
		 * {@inheritDoc}
		 * 
		 * @param aName The name involved in this exception.
		 */
		public NameException( String aMessage, String aName, Throwable aCause, String aErrorCode ) {
			super( aMessage, aCause, aErrorCode );
			_name = aName;
		}

		/**
		 * {@inheritDoc}
		 * 
		 * @param aName The name involved in this exception.
		 */
		public NameException( String aMessage, String aName, Throwable aCause ) {
			super( aMessage, aCause );
			_name = aName;
		}

		/**
		 * {@inheritDoc}
		 * 
		 * @param aName The name involved in this exception.
		 */
		public NameException( String aMessage, String aName ) {
			super( aMessage );
			_name = aName;
		}

		/**
		 * {@inheritDoc}
		 * 
		 * @param aName The name involved in this exception.
		 */
		public NameException( Throwable aCause, String aName, String aErrorCode ) {
			super( aCause, aErrorCode );
			_name = aName;
		}

		/**
		 * {@inheritDoc}
		 * 
		 * @param aName The name involved in this exception.
		 */
		public NameException( String aName, Throwable aCause ) {
			super( aCause );
			_name = aName;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public String getName() {
			return _name;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Object[] getPatternArguments() {
			return new Object[] { _name };
		}
	}

	/**
	 * The Class {@link PathException}.
	 */
	protected abstract static class PathException extends FileSystemException implements PathAccessor {

		private static final long serialVersionUID = 1L;

		protected String _path;

		/**
		 * {@inheritDoc}
		 * 
		 * @param aPath The path involved in this exception.
		 */
		public PathException( String aMessage, String aPath, String aErrorCode ) {
			super( aMessage, aErrorCode );
			_path = aPath;
		}

		/**
		 * {@inheritDoc}
		 * 
		 * @param aPath The path involved in this exception.
		 */
		public PathException( String aMessage, String aPath, Throwable aCause, String aErrorCode ) {
			super( aMessage, aCause, aErrorCode );
			_path = aPath;
		}

		/**
		 * {@inheritDoc}
		 * 
		 * @param aPath The path involved in this exception.
		 */
		public PathException( String aMessage, String aPath, Throwable aCause ) {
			super( aMessage, aCause );
			_path = aPath;
		}

		/**
		 * {@inheritDoc}
		 * 
		 * @param aPath The path involved in this exception.
		 */
		public PathException( String aMessage, String aPath ) {
			super( aMessage );
			_path = aPath;
		}

		/**
		 * {@inheritDoc}
		 *
		 * @param aPath The path involved in this exception.
		 */
		public PathException( String aPath, Throwable aCause, String aErrorCode ) {
			super( aCause, aErrorCode );
			_path = aPath;
		}

		/**
		 * {@inheritDoc}
		 *
		 * @param aPath The path involved in this exception.
		 */
		public PathException( String aPath, Throwable aCause ) {
			super( aCause );
			_path = aPath;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public String getPath() {
			return _path;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Object[] getPatternArguments() {
			return new Object[] { _path };
		}
	}
}
