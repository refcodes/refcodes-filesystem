// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.filesystem;

import org.refcodes.filesystem.FileSystemException.PathException;

/**
 * The Class {@link UnknownPathException}.
 */
public class UnknownPathException extends PathException {

	private static final long serialVersionUID = 1L;

	/**
	 * {@inheritDoc}
	 */
	public UnknownPathException( String aMessage, String aPath, String aErrorCode ) {
		super( aMessage, aPath, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public UnknownPathException( String aMessage, String aPath, Throwable aCause, String aErrorCode ) {
		super( aMessage, aPath, aCause, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public UnknownPathException( String aMessage, String aPath, Throwable aCause ) {
		super( aMessage, aPath, aCause );
	}

	/**
	 * {@inheritDoc}
	 */
	public UnknownPathException( String aMessage, String aPath ) {
		super( aMessage, aPath );
	}

	/**
	 * {@inheritDoc}
	 */
	public UnknownPathException( String aPath, Throwable aCause, String aErrorCode ) {
		super( aPath, aCause, aErrorCode );
	}

	/**
	 * {@inheritDoc}
	 */
	public UnknownPathException( String aPath, Throwable aCause ) {
		super( aPath, aCause );
	}
}
