// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.filesystem;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import org.refcodes.filesystem.FileHandle.MutableFileHandle;

/**
 * The change root space wrapper for a given {@link FileSystem} relocates the
 * paths accessed by an application to the given namespace. This is helpful in
 * case an application is to use a {@link FileSystem} sand box. Only the files
 * below that name space are visible by the file system.
 * <p>
 * ATTENTION: Make sure that the {@link FileSystem} cannot jail break its sand
 * box by using relative paths with ".." !!!
 */
public class ChangeRootFileSystemDecorator implements FileSystem {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final String _namespace;

	private final FileSystem _fileSystem;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new change root file system wrapper impl.
	 *
	 * @param aNamespace the namespace
	 * @param aFileSystem the file system
	 */
	public ChangeRootFileSystemDecorator( String aNamespace, FileSystem aFileSystem ) {
		_namespace = aNamespace;
		_fileSystem = aFileSystem;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasFile( String aKey ) throws IllegalKeyException, NoListAccessException, UnknownFileSystemException, IOException {
		aKey = FileSystemUtility.toNormalizedKey( aKey, this );
		aKey = _namespace + PATH_DELIMITER + aKey;
		return _fileSystem.hasFile( aKey );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasFile( String aPath, String aName ) throws IllegalPathException, IllegalNameException, NoListAccessException, UnknownFileSystemException, IOException {
		aPath = FileSystemUtility.toNormalizedPath( aPath, this );
		aName = FileSystemUtility.toNormalizedName( aName, this );
		aPath = _namespace + PATH_DELIMITER + aPath;
		return _fileSystem.hasFile( aPath, aName );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasFile( FileHandle aFileHandle ) throws NoListAccessException, UnknownFileSystemException, IOException, IllegalFileException {
		aFileHandle = FileSystemUtility.toNormalizedFileHandle( aFileHandle, this );
		final MutableFileHandle theMutableFileHandle = aFileHandle.toMutableFileHandle();
		theMutableFileHandle.setPath( toRealPath( aFileHandle ) );
		return _fileSystem.hasFile( theMutableFileHandle.toFileHandle() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FileHandle createFile( String aKey ) throws FileAlreadyExistsException, NoCreateAccessException, IllegalKeyException, UnknownFileSystemException, IOException, NoListAccessException {
		aKey = FileSystemUtility.toNormalizedKey( aKey, this );
		aKey = _namespace + PATH_DELIMITER + aKey;
		final FileHandle theFileHandle = _fileSystem.createFile( aKey );
		final MutableFileHandle theMutableFileHandle = theFileHandle.toMutableFileHandle();
		theMutableFileHandle.setPath( toVirtualPath( theFileHandle ) );
		return theMutableFileHandle.toFileHandle();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FileHandle createFile( String aPath, String aName ) throws FileAlreadyExistsException, NoCreateAccessException, IllegalNameException, IllegalPathException, UnknownFileSystemException, IOException, NoListAccessException {
		aPath = FileSystemUtility.toNormalizedPath( aPath, this );
		aName = FileSystemUtility.toNormalizedPath( aName, this );
		aPath = _namespace + PATH_DELIMITER + aPath;
		final FileHandle theFileHandle = _fileSystem.createFile( aPath, aName );
		final MutableFileHandle theMutableFileHandle = theFileHandle.toMutableFileHandle();
		theMutableFileHandle.setPath( toVirtualPath( theFileHandle ) );
		return theMutableFileHandle.toFileHandle();

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FileHandle getFileHandle( String aKey ) throws NoListAccessException, IllegalKeyException, UnknownFileSystemException, IOException, UnknownKeyException {
		aKey = FileSystemUtility.toNormalizedKey( aKey, this );
		aKey = _namespace + PATH_DELIMITER + aKey;
		final FileHandle theFileHandle = _fileSystem.getFileHandle( aKey );
		final MutableFileHandle theMutableFileHandle = theFileHandle.toMutableFileHandle();
		theMutableFileHandle.setPath( toVirtualPath( theFileHandle ) );
		return theMutableFileHandle.toFileHandle();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FileHandle getFileHandle( String aPath, String aName ) throws NoListAccessException, IllegalNameException, IllegalPathException, UnknownFileSystemException, IOException, UnknownKeyException {
		aPath = FileSystemUtility.toNormalizedPath( aPath, this );
		aName = FileSystemUtility.toNormalizedName( aName, this );
		aPath = _namespace + PATH_DELIMITER + aPath;
		final FileHandle theFileHandle = _fileSystem.getFileHandle( aPath, aName );
		final MutableFileHandle theMutableFileHandle = theFileHandle.toMutableFileHandle();
		theMutableFileHandle.setPath( toVirtualPath( theFileHandle ) );
		return theMutableFileHandle.toFileHandle();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void fromFile( FileHandle aFromFileHandle, OutputStream aOutputStream ) throws ConcurrentAccessException, UnknownFileException, NoReadAccessException, UnknownFileSystemException, IOException, NoListAccessException, IllegalFileException {
		aFromFileHandle = FileSystemUtility.toNormalizedFileHandle( aFromFileHandle, this );
		final MutableFileHandle theMutableFromFileHandle = aFromFileHandle.toMutableFileHandle();
		theMutableFromFileHandle.setPath( toRealPath( aFromFileHandle ) );
		_fileSystem.fromFile( theMutableFromFileHandle.toFileHandle(), aOutputStream );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void toFile( FileHandle aToFileHandle, InputStream aInputStream ) throws ConcurrentAccessException, UnknownFileException, NoWriteAccessException, UnknownFileSystemException, IOException, NoListAccessException, IllegalFileException {
		aToFileHandle = FileSystemUtility.toNormalizedFileHandle( aToFileHandle, this );
		final MutableFileHandle theMutableToFileHandle = aToFileHandle.toMutableFileHandle();
		theMutableToFileHandle.setPath( toRealPath( aToFileHandle ) );
		_fileSystem.toFile( theMutableToFileHandle.toFileHandle(), aInputStream );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public InputStream fromFile( FileHandle aFromFileHandle ) throws ConcurrentAccessException, UnknownFileException, NoReadAccessException, UnknownFileSystemException, IOException, NoListAccessException, IllegalFileException {
		aFromFileHandle = FileSystemUtility.toNormalizedFileHandle( aFromFileHandle, this );
		final MutableFileHandle theMutableFromFileHandle = aFromFileHandle.toMutableFileHandle();
		theMutableFromFileHandle.setPath( toRealPath( aFromFileHandle ) );
		return _fileSystem.fromFile( theMutableFromFileHandle.toFileHandle() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public OutputStream toFile( FileHandle aToFileHandle ) throws ConcurrentAccessException, UnknownFileException, NoWriteAccessException, UnknownFileSystemException, IOException, IllegalFileException {
		aToFileHandle = FileSystemUtility.toNormalizedFileHandle( aToFileHandle, this );
		final MutableFileHandle theMutableToFileHandle = aToFileHandle.toMutableFileHandle();
		theMutableToFileHandle.setPath( toRealPath( aToFileHandle ) );
		return _fileSystem.toFile( theMutableToFileHandle.toFileHandle() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void fromFile( FileHandle aFromFileHandle, File aToFile ) throws ConcurrentAccessException, UnknownFileException, NoReadAccessException, UnknownFileSystemException, IOException, NoListAccessException, IllegalFileException {
		aFromFileHandle = FileSystemUtility.toNormalizedFileHandle( aFromFileHandle, this );
		final MutableFileHandle theMutableFromFileHandle = aFromFileHandle.toMutableFileHandle();
		theMutableFromFileHandle.setPath( toRealPath( aFromFileHandle ) );
		_fileSystem.fromFile( theMutableFromFileHandle.toFileHandle(), aToFile );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void toFile( FileHandle aToFileHandle, File aFromFile ) throws ConcurrentAccessException, UnknownFileException, NoWriteAccessException, UnknownFileSystemException, IOException, NoListAccessException, IllegalFileException {
		aToFileHandle = FileSystemUtility.toNormalizedFileHandle( aToFileHandle, this );
		final MutableFileHandle theMutableToFileHandle = aToFileHandle.toMutableFileHandle();
		theMutableToFileHandle.setPath( toRealPath( aToFileHandle ) );
		_fileSystem.toFile( theMutableToFileHandle.toFileHandle(), aFromFile );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void toFile( FileHandle aToFileHandle, byte[] aBuffer ) throws ConcurrentAccessException, UnknownFileException, NoWriteAccessException, UnknownFileSystemException, IOException, NoListAccessException, IllegalFileException {
		aToFileHandle = FileSystemUtility.toNormalizedFileHandle( aToFileHandle, this );
		final MutableFileHandle theMutableToFileHandle = aToFileHandle.toMutableFileHandle();
		theMutableToFileHandle.setPath( toRealPath( aToFileHandle ) );
		_fileSystem.toFile( theMutableToFileHandle.toFileHandle(), aBuffer );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FileHandle renameFile( FileHandle aFileHandle, String aNewName ) throws UnknownFileException, ConcurrentAccessException, FileAlreadyExistsException, NoCreateAccessException, NoDeleteAccessException, IllegalNameException, UnknownFileSystemException, IOException, NoListAccessException, IllegalFileException {
		aFileHandle = FileSystemUtility.toNormalizedFileHandle( aFileHandle, this );
		final MutableFileHandle theMutableFileHandle = aFileHandle.toMutableFileHandle();
		theMutableFileHandle.setPath( toRealPath( aFileHandle ) );
		final FileHandle theNewFileHandle = _fileSystem.renameFile( theMutableFileHandle.toFileHandle(), aNewName );
		final MutableFileHandle theMutableNewFileHandle = theNewFileHandle.toMutableFileHandle();
		theMutableNewFileHandle.setPath( toVirtualPath( theNewFileHandle ) );
		return theMutableNewFileHandle.toFileHandle();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FileHandle moveFile( FileHandle aFileHandle, String aNewKey ) throws UnknownFileException, ConcurrentAccessException, FileAlreadyExistsException, NoCreateAccessException, NoDeleteAccessException, IllegalKeyException, UnknownFileSystemException, IOException, NoListAccessException, IllegalFileException {
		aFileHandle = FileSystemUtility.toNormalizedFileHandle( aFileHandle, this );
		final MutableFileHandle theMutableFileHandle = aFileHandle.toMutableFileHandle();
		theMutableFileHandle.setPath( toRealPath( aFileHandle ) );
		aNewKey = _namespace + PATH_DELIMITER + aNewKey;
		final FileHandle theNewFileHandle = _fileSystem.moveFile( theMutableFileHandle.toFileHandle(), aNewKey );
		final MutableFileHandle theMutableNewFileHandle = theNewFileHandle.toMutableFileHandle();
		theMutableNewFileHandle.setPath( toVirtualPath( theNewFileHandle ) );
		return theMutableNewFileHandle.toFileHandle();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteFile( FileHandle aFileHandle ) throws ConcurrentAccessException, UnknownFileException, NoDeleteAccessException, UnknownFileSystemException, IOException, NoListAccessException, IllegalFileException {
		aFileHandle = FileSystemUtility.toNormalizedFileHandle( aFileHandle, this );
		final MutableFileHandle theMutableFileHandle = aFileHandle.toMutableFileHandle();
		theMutableFileHandle.setPath( toRealPath( aFileHandle ) );
		_fileSystem.deleteFile( theMutableFileHandle.toFileHandle() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasFiles( String aPath, boolean isRecursively ) throws NoListAccessException, IllegalPathException, UnknownFileSystemException, IOException {
		aPath = FileSystemUtility.toNormalizedPath( aPath, this );
		aPath = _namespace + PATH_DELIMITER + aPath;
		return _fileSystem.hasFiles( aPath, isRecursively );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<FileHandle> getFileHandles( String aPath, boolean isRecursively ) throws NoListAccessException, UnknownPathException, IllegalPathException, UnknownFileSystemException, IOException {
		aPath = FileSystemUtility.toNormalizedPath( aPath, this );
		aPath = _namespace + PATH_DELIMITER + aPath;
		final List<FileHandle> theFileHandles = _fileSystem.getFileHandles( aPath, isRecursively );
		final List<FileHandle> theFoundFileHandles = new ArrayList<>();
		MutableFileHandle eMutableFileHandle;
		for ( FileHandle eFileHandle : theFileHandles ) {
			eMutableFileHandle = eFileHandle.toMutableFileHandle();
			eMutableFileHandle.setPath( toVirtualPath( eFileHandle ) );
			theFoundFileHandles.add( eMutableFileHandle.toFileHandle() );
		}
		return theFoundFileHandles;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void destroy() {
		if ( _fileSystem != null ) {
			_fileSystem.destroy();
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Removes the namespace from the {@link FileHandle}'s path.
	 * 
	 * @param aFileHandle The {@link FileHandle} from which to remove the
	 *        namespace. This is the file handle used by the virtual file
	 *        system.
	 * 
	 * @return The path from the provided {@link FileHandle} without the
	 *         namespace.
	 */
	private String toVirtualPath( FileHandle aFileHandle ) {
		return aFileHandle.getPath().substring( ( _namespace + PATH_DELIMITER ).length() );
	}

	/**
	 * Adds the namespace to the {@link FileHandle}'s path.
	 * 
	 * @param aFileHandle The {@link FileHandle} to which to add the namespace.
	 *        This is the file handle used by the real file system.
	 * 
	 * @return The path from the provided {@link FileHandle} with the namespace
	 *         prepended.
	 */
	private String toRealPath( FileHandle aFileHandle ) {
		return _namespace + PATH_DELIMITER + aFileHandle.getPath();
	}
}
