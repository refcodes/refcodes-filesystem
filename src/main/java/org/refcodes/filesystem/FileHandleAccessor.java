// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.filesystem;

/**
 * Provides an accessor for a file handle property.
 */
public interface FileHandleAccessor {

	/**
	 * Retrieves the file handle from the file property.
	 * 
	 * @return The file handle stored by the file handle property.
	 */
	FileHandle getFileHandle();

	/**
	 * Provides a mutator for a file handle property.
	 */
	public interface FileHandleMutator {

		/**
		 * Sets the file handle for the file handle property.
		 * 
		 * @param aHandle The file handle to be stored by the file handle
		 *        property.
		 */
		void setFileHandle( String aHandle );
	}

	/**
	 * Provides a builder method for a file handle property returning the
	 * builder for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface FileHandleBuilder<B extends FileHandleBuilder<B>> {

		/**
		 * Sets the file handle for the file handle property.
		 * 
		 * @param aHandle The file handle to be stored by the file handle
		 *        property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withFileHandle( String aHandle );
	}

	/**
	 * Provides a file handle property.
	 */
	public interface FileHandleProperty extends FileHandleAccessor, FileHandleMutator {

		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given file handle (setter) as
		 * of {@link #setFileHandle(String)} and returns the very same value
		 * (getter) being produced to a {@link FileHandle} (as of
		 * {@link #getFileHandle()}).
		 * 
		 * @param aHandle The file to set (via {@link #setFileHandle(String)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default FileHandle letFileHandle( String aHandle ) {
			setFileHandle( aHandle );
			return getFileHandle();
		}
	}
}
