// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.filesystem;

/**
 * Provides an accessor for a {@link FileSystem} property.
 */
public interface FileSystemAccessor {

	/**
	 * Gets the file system stored by this property.
	 * 
	 * @return The file system stored by this property.
	 */
	FileSystem getFileSystem();

	/**
	 * Provides a mutator for a {@link FileSystem} property.
	 */
	public interface FileSystemMutator {

		/**
		 * Sets the file system for this property.
		 * 
		 * @param aFileSystem The file system of this property.
		 */
		void setFileSystem( FileSystem aFileSystem );
	}

	/**
	 * Provides a {@link FileSystem} property.
	 */
	public interface FileSystemProperty extends FileSystemAccessor, FileSystemMutator {
		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given {@link FileSystem}
		 * (setter) as of {@link #setFileSystem(FileSystem)} and returns the
		 * very same value (getter).
		 * 
		 * @param aFileSystem The {@link FileSystem} to set (via
		 *        {@link #setFileSystem(FileSystem)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default FileSystem letFileSystem( FileSystem aFileSystem ) {
			setFileSystem( aFileSystem );
			return aFileSystem;
		}
	}
}
