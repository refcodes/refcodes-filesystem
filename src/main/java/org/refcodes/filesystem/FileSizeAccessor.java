// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.filesystem;

/**
 * Provides an accessor for a size property.
 */
public interface FileSizeAccessor {

	/**
	 * Retrieves the size from the size property.
	 * 
	 * @return The size stored by the size property.
	 */
	long getFileSize();

	/**
	 * Provides a mutator for a size property.
	 */
	public interface FileSizeMutator {

		/**
		 * Sets the size for the size property.
		 * 
		 * @param aSize The size to be stored by the size property.
		 */
		void setFileSize( long aSize );
	}

	/**
	 * Provides a builder method for a file size property returning the builder
	 * for applying multiple build operations.
	 * 
	 * @param <B> The builder to return in order to be able to apply multiple
	 *        build operations.
	 */
	public interface FileSizeBuilder<B extends FileSizeBuilder<B>> {

		/**
		 * Sets the file size for the file size property.
		 * 
		 * @param aFileSize The file size to be stored by the file size
		 *        property.
		 * 
		 * @return The builder for applying multiple build operations.
		 */
		B withFileSize( String aFileSize );
	}

	/**
	 * Provides a size property.
	 */
	public interface FileSizeProperty extends FileSizeAccessor, FileSizeMutator {
		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given file size (setter) as of
		 * {@link #setFileSize(long)} and returns the very same value (getter).
		 * 
		 * @param aFileSize The file size to set (via
		 *        {@link #setFileSize(long)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default long letFileSize( long aFileSize ) {
			setFileSize( aFileSize );
			return aFileSize;
		}
	}
}
