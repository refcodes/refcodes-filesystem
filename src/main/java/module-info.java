module org.refcodes.filesystem {
	requires org.refcodes.data;
	requires transitive org.refcodes.component;
	requires transitive org.refcodes.exception;
	requires transitive org.refcodes.mixin;
	requires java.logging;

	exports org.refcodes.filesystem;
}
